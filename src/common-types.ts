export interface IPost {
    label?: string
    descr?: string
    date?: Date
    userId?: string
}
export interface IPost2 {
    label2: string
}

export interface IPosts {
    first: IPost
    second: IPost2
}
