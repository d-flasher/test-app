import { fireEvent, render } from '@testing-library/react'
import { act } from 'react-dom/test-utils'

import { MobXTest2 } from './mobx-test'
import { MobXTestCompChild } from './MobXTestComp'

test('MobxTestCompChild should view MobxTest2', () => {
    const { findByRole, queryByText, rerender, queryByRole, queryAllByRole, debug, getAllByRole } = render(<MobXTestCompChild></MobXTestCompChild>)

    expect(queryByText(/not found/i)).toBeInTheDocument()
    expect(queryByRole('list')).not.toBeInTheDocument()

    const data = new MobXTest2()
    rerender(<MobXTestCompChild v={data}></MobXTestCompChild>)
    expect(queryByText(/not found/i)).not.toBeInTheDocument()

    const field2El = queryAllByRole('listitem').at(1)
    expect(field2El).toHaveTextContent(/^field2: 0$/)
    // expect(queryAllByRole('listitem').at(2)).toHaveTextContent(/^field3: 0$/)

    // act(() => {
    fireEvent.click(queryByRole('button') as HTMLButtonElement)
    // })

    expect(field2El).toHaveTextContent(/^field2: 1$/)
    // expect(queryAllByRole('listitem').at(2)).toHaveTextContent(/^field3: 1$/)

    data.setField2(data.field2 + 1)
    expect(field2El).toHaveTextContent(/^field2: 2$/)

    // setTimeout(() => {
    // console.log(2, debug())
    // })

    // expect(getAllByRole('listitem').at(1)).toHaveTextContent(/^field2: 1$/)


    /* const listEl = queryByRole('list')
    expect(listEl).toBeInTheDocument()

    data.setField1('qwerty')
    console.log(7, data.field1)
    // expect(await findByText('qwerty')).toBeInTheDocument()
    // await waitFor(() => expect(queryByText('qwerty')).toBeInTheDocument())
    const t = queryByText('qwerty')
    console.log(1, t)
    findByText('qwerty').then(v => console.log(3, v))
    setTimeout(() => {
        const t = queryByText('qwerty')
        console.log(2, t)
    }, 600) */
})
