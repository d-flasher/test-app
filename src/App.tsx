import './App.css'

import { FC, PropsWithChildren, ReactNode, useEffect } from 'react'
import { BrowserRouter, Outlet, Route, Routes, useLocation, useParams } from 'react-router-dom'

import { IWord } from './api'
import { MobXTest, MobXTest2 } from './mobx-test'
import MobXTestComp from './MobXTestComp'
import PageNotFound from './PageNotFountd'
import { PostData } from './posts'

const words: IWord[] = [
    { id: '1', value: 'word1' },
    { id: '2', value: 'word2' },
    { id: '3', value: 'word3' },
    { id: '4', value: 'word4' },
]

const post = new PostData()

type PropsWidthId = PropsWithChildren<{ id: string }>

const TeamFC: FC<PropsWidthId> = props => {
    return <div>Team</div>
}

const getRouterId = (Component: FC<PropsWidthId>) => {
    const ComponentWithRouterProp = (props: PropsWithChildren<any>) => {
        let params = useParams<Omit<PropsWidthId, 'children'>>()
        return (
            <Component
                {...props}
                id={params.id}
            />
        )
    }
    return ComponentWithRouterProp
}
const TeamFC_router = getRouterId(TeamFC)

const mobXData = new MobXTest()
const mobXTest2 = new MobXTest2()
mobXTest2.setField2(1)
mobXData.setField3(mobXTest2)

for (let i = 1; i <= 4; i++) {
    const mobXTest21 = new MobXTest2()
    mobXTest21.setField2(i)
    mobXData.pushField4(mobXTest21)
}

const App = () => {
    return (
        <>
            <MobXTestComp v={mobXData}></MobXTestComp>

            <BrowserRouter>
                <AppContent>
                    <Routes>
                        <Route path="/" element={<IndexPage></IndexPage>}>
                            <Route index element={<div>App index</div>} />
                            <Route path="teams">
                                <Route path=":id" element={<TeamFC_router></TeamFC_router>} />
                                <Route path="new" element={<div>Team new</div>} />
                                <Route index element={<div>Teams index</div>} />
                            </Route>
                            <Route
                                path="*"
                                element={<PageNotFound></PageNotFound>}
                            />
                        </Route>
                    </Routes>
                </AppContent>
            </BrowserRouter>
        </>
    )
}

const IndexPage: FC = () => {
    return <div>
        Index:
        <Outlet></Outlet>
    </div>
}

const AppContent: FC = ({ children }) => {

    let location = useLocation()

    useEffect(() => {
        console.log(8, location)
    }, [location])

    return <>{children}</>
}

export default App
