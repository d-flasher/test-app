import { faker } from '@faker-js/faker'
import { makeAutoObservable } from 'mobx'

import { IPost } from './common-types'

export class PostData implements IPost {
    constructor() {
        makeAutoObservable(this)
    }

    date?: Date = faker.datatype.datetime()
    descr?: string = faker.lorem.paragraphs()
    label?: string = faker.name.findName()
    userId?: string = faker.datatype.uuid()
    setDate(value: string) { this.date = new Date() }
}
