import { render, screen } from '@testing-library/react'
import { FC } from 'react'

const createEl = () => {
  const res = document.createElement('div')
  const child = document.createElement('div')
  child.setAttribute('data-testid', 'me')
  child.classList.add('me-class')
  child.innerHTML = '1'
  res.append(child)
  return res
}

const testReactEl = () => (
  <div>
    <div data-testid='me' className='me-class'>
      1
    </div>
  </div>
)

test('test', () => {
  // const el = createEl()
  const { getByTestId } = render(testReactEl())
  const target = getByTestId('me')
  expect(target).toBeInTheDocument()
})

test('test2', () => {
  // const el = createEl()
  const { getByTestId } = render(testReactEl())
  const target = getByTestId('me')
  expect(target).toHaveClass('me-class')
  expect(target).toHaveTextContent('1')
})
