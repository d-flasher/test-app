import { render } from '@testing-library/react'

import { IWord } from './api'
import Word from './Word'

test('should shows word', () => {
    const word: IWord = { id: '1', value: 'word1' }
    const { getByRole } = render(<Word word={word}></Word>)
    const el = getByRole('definition')
    expect(el).toBeInTheDocument()
    expect(el).not.toHaveTextContent('word2')
    expect(el).toHaveTextContent('word1')
})
