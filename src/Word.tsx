import { FC } from 'react'

import { IWord } from './api'

interface IWord_FC {
    word: IWord
}

const Word: FC<IWord_FC> = ({ word }) => {
    return (
        <div>
            <b>Word: </b> <span role='definition'>{word.value}</span>
        </div>
    )
}
export default Word
