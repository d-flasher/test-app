import { makeAutoObservable } from 'mobx'

export class MobXTest {
    constructor() {
        makeAutoObservable(this)
    }

    field1?: string = 'field1'
    setField1(v: string) { this.field1 = v }

    field2?: number = 2
    setField2(v: number) { this.field2 = v }

    field3?: MobXTest2
    setField3(v: MobXTest2) { this.field3 = v }

    field4: MobXTest2[] = []
    pushField4(v: MobXTest2) { this.field4?.push(v) }
}

export class MobXTest2 {
    constructor() {
        makeAutoObservable(this)
    }

    field1?: string
    setField1(v: string) { this.field1 = v }

    field2: number = 0
    setField2(v: number) { this.field2 = v }
}
