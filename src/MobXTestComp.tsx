import { observer } from 'mobx-react-lite'
import { FC, useState } from 'react'

import { MobXTest, MobXTest2 } from './mobx-test'

const MobXTestComp: FC<{ v: MobXTest }> = ({ v }) => {
    const onField2Click = () => {
        if (v.field2) v.setField2(v.field2 + 1)
        if (v.field3?.field2) v.field3.setField2(v.field3.field2 + 1)
        if (v.field4[1].field2) v.field4[1].setField2(v.field4[1].field2 + 1)
    }

    const f32 = v.field3?.field2

    return <ul>
        <li>field1: {v.field1}</li>
        <li>field2: {v.field2}</li>
        <li>f32: {f32}</li>
        <li><MobXTestCompChild v={v.field3}></MobXTestCompChild></li>
        <li><MobXTestCompChildArr v={v.field4}></MobXTestCompChildArr></li>
        <li>
            <button onClick={onField2Click}>+</button>
        </li>
    </ul>
}

export const MobXTestCompChildArr: FC<{ v?: MobXTest2[] }> = observer(({ v }) => {
    if (!v) return <p>Not folund</p>
    return <ol>{v.map((item, index) => (
        <li key={index}>
            <MobXTestCompChild v={item}></MobXTestCompChild>
        </li>
    ))}</ol>
})

export const MobXTestCompChild: FC<{ v?: MobXTest2 }> = observer(({ v }) => {
    // const [field3, setField3] = useState(0)

    if (!v) return <p>Not found</p>

    const onField2Click = () => {
        if (!Number.isNaN(v.field2)) v.setField2(v.field2 + 1)
        // setField3(field3 + 1)
    }

    return <ul>
        <li>field1: {v.field1}</li>
        <li>field2: {v.field2}</li>
        {/* <li>field3: {field3}</li> */}
        <li>
            <button onClick={onField2Click}>+</button>
        </li>
    </ul>
})

export default observer(MobXTestComp)
