import { inject } from 'mobx-react'
import { FC } from 'react'

import { IPost } from './common-types'
import { PostData } from './posts'

const Post: FC<IPost> = ({ label, descr, date, userId }) => {
    return <div role='article'>
        <h1>{label}</h1>
        <p>{descr}</p>
        <div>
            Date: <span>{date?.toLocaleDateString()}</span>
        </div>
        <div>
            User: <span>{userId}</span>
        </div>
    </div>
}

export default inject((stores: { post: PostData }) => {
    console.log(stores)
    return stores.post
})(Post)
