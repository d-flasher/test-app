import { FC } from 'react'

const PageNotFound: FC = () => (
    <h1>404</h1>
)
export default PageNotFound
