import { render } from '@testing-library/react'

import { IWord } from './api'
import Words from './Words'

test('should show words list', () => {
    const words: IWord[] = [
        { id: '1', value: 'Hello!' },
        { id: '2', value: 'World' },
    ]

    const { getAllByRole } = render(<Words words={words}></Words>)
    const wordsEls = getAllByRole('listitem', { name: 'word-item' })
    expect(wordsEls).toHaveLength(2)
    expect(wordsEls[0]).toBeVisible()
    expect(wordsEls[0]).toHaveTextContent('Hello!')
    expect(wordsEls[1]).toHaveTextContent(/world/i)
})
