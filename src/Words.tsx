import { FC } from 'react'

import { IWord } from './api'
import Word from './Word'

interface IWords_FC {
    words: IWord[]
}

const Words: FC<IWords_FC> = ({ words }) => {
    const mappedWords = words.map(i => (
        <li key={i.id} aria-label="word-item">
            <Word word={i}></Word>
        </li>
    ))
    return (
        <ul>
            {mappedWords}
        </ul>
    )
}
export default Words
